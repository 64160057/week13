package com.watcharaphon.swingtutorial;

import javax.swing.*;

class MyFrame extends JFrame {

    JButton button;

    public MyFrame() {
        super("First JFrame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        this.setLayout(null);
        this.add(button);
        this.setSize(400, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}

public class Simple2 {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
    }
}
